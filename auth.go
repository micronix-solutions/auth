/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package auth

import (
	"errors"
)

type AuthenticatedUser struct {
	Username  string
	Fullname  string
	Firstname string
	Lastname  string
	Email     string
}

type Authenticater interface {
	Authenticate(username string, password string) (*AuthenticatedUser, error)
}

type InMemoryAuthenticater struct {
	credentials map[string]string
}

func (me *InMemoryAuthenticater) ValidateCredentials(username string, password string) (*AuthenticatedUser, error) {
	pw, exists := me.credentials[username]
	if !exists {
		return nil, errors.New("No user found for that username")
	} else if pw != password {
		return nil, errors.New("Invalid password")
	}
	return &AuthenticatedUser{username, "whatever youwant", "whatever", "youwant", "joe.user@micronix.solutions"}, nil
}
